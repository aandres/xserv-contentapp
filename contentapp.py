#!/usr/bin/python


import socket

class Contentapp:

    def parse (self, request):
        #"""Parse the received request, extracting the relevant information."""
        recurso = request.decode('utf-8').split(' ')[1]
        return recurso

    def process (self, parsedRequest):
        """Process the relevant elements of the request.

        Returns the HTTP code for the reply, and an HTML page.
        """
        lista = {'/twitter': 'https://labs.etsit.urjc.es/', '/twitch': 'https://twitch.tv/',
                '/youtube': 'https://www.youtube.com/', '/instagram': 'https://www.instagram.com/'}
        state = False
        for key in lista:
            if key == str(parsedRequest):
                state = True
                print(str(lista[key]))
                return ("301 Moved permanently \r\n", 'Location: ' + str(lista[key]))
        if not state:
            return ("200 OK \r\n\r\n", "<html><body><h1>Introduzca uno de los siguientes recursos"
                                       ":</h1><p>/twitter || /twitch || /youtube || /instagram</p></body></html>")

    def __init__(self, hostname, port):
        """Initialize the web application."""

        # Create a TCP objet socket and bind it to a port
        mySocket = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
        mySocket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        mySocket.bind((hostname, port))

        # Queue a maximum of 5 TCP connection requests
        mySocket.listen(5)

        # Accept connections, read incoming data, and call
        # parse and process methods (in a loop)

        while True:
            print('Waiting for connections')
            (recvSocket, address) = mySocket.accept()
            print('HTTP request received (going to parse and process):')
            request = recvSocket.recv(2048)
            print(request)
            parsedRequest = self.parse(request)
            (returnCode, htmlAnswer) = self.process(parsedRequest)
            print('Answering back...')

            recvSocket.send(b'HTTP/1.1 ' + bytes(returnCode, 'utf-8') +
                            bytes(htmlAnswer,'utf-8') + b'\r\n')

            recvSocket.close()

if __name__ == "__main__":
    testWebApp = Contentapp("localhost", 1234)
